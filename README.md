# Embedme项目简介

Embedme是一个基于嵌入式Linux的具有可移植性的C++程序开发框架，其目的是为了加快Linux应用程序的开发速度，解放程序员的大脑和双手，让大家把精力投入到更有意义的事情当中去。  

Embedme提供了包括:  
跟踪打印(Tracer)、日志(Logger)、线程(ThreadUtil)、协程(Coroutine)、进程(ProcUtil)、校验(CheckUtil)、通用工具(ComUtil)、定时器(Timer)、消息队列(MsgQueue)、数据缓存(DataBuffer)、文件(FileUtil)、时间日期(DateTime)、字符串工具(StrUtil)、正则匹配(RegExp)、串口(SerialPort)、网络通信(Socket)、IO复用(Pollset)、元组(Tuple)、CAN通信(CANSocket)、插件(Pluglet)、有限状态机(FSMachine)、内存管理(MemUtil)、数学工具(MathUtil)、单元测试(CppUnitLite)等嵌入式开发中常用的模块。  

在Embedme项目中集成了非常多优秀的开源库：  
CANOpenNode,ctemplate,cJSON,eigen,libconfig++,curl,libev,libgif,libmqttclient,libsdbus++,libsocketcan,sqlite,tinyalsa,tinyxml,tinyxml2,yaml-cpp,libev,spdlog,sqlite_orm，感谢这些开源库的作者的无私奉献。  

Embedme目前还在不断完善中，欢迎各位同行fork本开源库，也期待您的建议和开源贡献。

## 作者

有任何问题，欢迎联系： cblock@126.com 牛咕噜大人

## 开源协议

本软件遵循MIT协议,请自觉遵守该协议,如果您使用此源码,请务必保留README在您的工程代码目录下!

## 工程结构

* app: 应用程序源代码存放目录.
* example: 工程示例源码目录.
* opensource: 开源软件库目录.
* opensource/libemb: libemb库源码,此文件夹内的代码不依赖第三方开源库.
* opensource/libembx: libembx库源码,此文件夹内的代码会依赖第三方开源库.
* prebuild: mbuild编译系统目录.
* test_case: 测试样例.
* tools: 工具.

## 编译系统mbuild system

本工程采用mbuild系统进行编译,mbuild系统的使用请参考:[mbuild使用说明](prebuild/README.md)

* mbuild借鉴了Android编译系统.
* 支持编译库、可执行程序、开源代码、Qt工程.
* 支持自动创建工程文件(mbuild_project).
* 支持创建CMake工程(mbuild_cmake_project).
* 支持交叉编译及多平台编译.

### 编译说明

在编译前请先确认已安装autoconf,automake,libtool等工具,否则无法编译成功,如遇编译错误，请自行查看错误提示，判断是否是工具未安装。

1 . cd到工程跟目录下

```bash
$cd embedme
```

2 . 设置mbuild编译环境

```bash
$source prebuild/envsetup.sh
```

3 . 设置编译目标体系

```bash
$mbuild_setup
```

4 . 编译

```bash
$mbuild_make opensource/libemb libemb
$mbuild_make test_case/test_libemb test_libemb
$mbuild_make app/demo demo
```

#### CMAKE编译

当前已支持使用cmake编译，你可以为子工程手动创建CMakeLists.txt或借助mbuild_cmake_project自动创建CMakeLists.txt

```bash
$ mbuild_cmake_project app/test test # 将会在app/test目录下自动生成CMakeLists.txt
$ vim app/test/CMakeLists.txt #需要手动编写CMakeLists.txt
$ mkdir build_dir # 创建cmake构建目录
$ cd build_dir   # 在build_dir目录下编译工程
$ cmake ..
$ make 
$ make install
```

## API文档

API文档请参考[manual/README.md](manual/README.md)
