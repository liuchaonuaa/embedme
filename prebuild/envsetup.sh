#!/bin/sh
##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##  WARNING:本文件为mbuild系统环境配置脚本，修改时务必谨慎! ##
##############################################################
PROJECT_ROOT=$PWD

#检查环境是否正确
if [ -f "$PROJECT_ROOT/prebuild/core/mbuild-core.sh" -a -f "$PROJECT_ROOT/prebuild/envsetup.sh" -a -f "$PROJECT_ROOT/prebuild/core/main.mk" ]; then
echo -e "\033[33m\033[1m"
cat <<EOF
-------------------------------------------------------------------------------
  [ mbuild system help ] <20190918-v2.1>
  --------------------------------------
  Invoke "source ./prebuild/envsetup.sh" from your shell to add the following 
  functions to your environment:
- mbuild_setup: set product and host for build envoriment.
- mbuild_project <directory> <target> [exec|lib|res|qt] : create project for target in the directory.
- mbuild_make <directory> <target> <linktype> : make target from the directory.
- mbuild_clean <directory> <target> : make clean target from the directory.
- mbuild_remake <directory> <target> <linktype> : mclean and mbuild.
- mbuild_auto: generate autobuild.sh for automatic building.
- mbuild_automake <linktype> : build all with linktype
- mbuild_autoclean: clean all targets
- mbuild_quit: quit and reset environment 
  -----------------------------------------------------------------------------
  1. Commands only can work well once you hava a <target>.mk file in the target
     source directory.
  2. <linktype> has two type: static and shared, if you donot specify this 
     value, you should set LOCAL_LIB_TYPE in <target>.mk 
-------------------------------------------------------------------------------
EOF
echo -e "\033[0m"
else
	echo -e '\033[33m\033[1mYou should invoke ". prebuild/envsetup.sh" in project top directory!'
	echo -e "come back to $HOME\033[0m"
	exit 1
fi

source $PROJECT_ROOT/prebuild/core/mbuild-core.sh
source $PROJECT_ROOT/prebuild/profile.sh

function mbuild_setup()
{
    PS1="\[\e[1;33m\]mbuild@\[\e[0m\]\[\e[1;35m\]$HOST_FNAME:\[\e[0m\]\[\e[1;36m\]\W@\[\e[0m\]"
    SYSROOT_PATH=
    # 该命令可以带参数,用于指定sysroot,product和target
    if [ $# -eq 0 ];then
        set_product
        set_target
    elif [ $# -eq 1 -o $# -eq 3 ];then
        if [ -d $1 ];then
            SYSROOT_PATH=`cd $(dirname $1); pwd`            
        else
            SYSROOT_PATH=
        fi
        if [ $# -eq 3 ];then
            set_product $2
            set_target $3
        else
            set_product
            set_target
        fi
    elif [ $# -eq 2 ];then
        set_product $1
        set_target $2
    else
        echo "[usage]"
        echo "mbuild_setup"
        echo "mbuild_setup <sysroot-path>"
        echo "mbuild_setup <product-id> <target-id>"
        echo "mbuild_setup <sysroot-path> <product-id> <target-id>"
        return
    fi

    # 创建目标文件夹
    if [ ! -d "$PROJECT_ROOT/output-$HOST_FNAME/bin" ];then
        mkdir -p $PROJECT_ROOT/output-$HOST_FNAME/bin
    fi
    if [ ! -d "$PROJECT_ROOT/output-$HOST_FNAME/lib" ];then
        mkdir -p $PROJECT_ROOT/output-$HOST_FNAME/lib
    fi
    if [ ! -d "$PROJECT_ROOT/output-$HOST_FNAME/include" ];then
        mkdir -p $PROJECT_ROOT/output-$HOST_FNAME/include
    fi

    PNAME="$(echo $PRODUCT_NAME | tr '[:lower:]' '[:upper:]')"
    if [ "$PNAME" == "DEFAULT" ];then
		echo -e "==>mbuild setup product default (not set).\033[0m"
	else
        echo -e "==>mbuild setup product: PRODUCT_$PNAME."
    fi
    if [ "$HOST " == " " ];then
        echo -e "==>mbuild setup target: $HOST_FNAME gcc"
    else
        echo -e "==>mbuild setup target: $HOST_FNAME $HOST-gcc"
    fi
    echo -e "==>mbuild setup ok."
}

function mbuild_quit()
{
    PS1="\u@\h:\W\$"
}

function mbuild_auto()
{
    if [ ! -f "$PROJECT_ROOT/autobuild.sh" ];then
        cp $PROJECT_ROOT/prebuild/templates/autobuild.sh.in $PROJECT_ROOT/autobuild.sh
        echo "Generated $PROJECT_ROOT/autobuild.sh"
    else
        echo "autobuild.sh allready exists!"
    fi
    echo "you can execute autobuild.sh for automatic building now."
}
