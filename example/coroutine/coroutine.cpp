#include <libemb/Socket.h>
#include <libemb/Tracer.h>
#include <libemb/FileUtil.h>
#include <libemb/StrUtil.h>
#include <libemb/ArgUtil.h>
#include <libemb/DateTime.h>
#include <libemb/Coroutine.h>
#include <libemb/Thread.h>

using namespace libemb;

class CoroutinePing:public Coroutine{
public:
	void routine()
	{
		while(isRunning())
		{
			TRACE_CYAN("coroutine ping... [%s]",CSTR(DateTime::getDateTime().toString()));
			msleep(1000);
			//yield();
		}
	}
};

class CoroutinePong:public Coroutine{
public:
	void routine()
	{
		while(isRunning())
		{
			TRACE_GREEN("coroutine pong... [%s]",CSTR(DateTime::getDateTime().toString()));
			msleep(2000);
			//yield();
		}
	}
};


int main(int argc, char* argv[])
{
	std::string argValue;
	ArgOption  option;
	Tracer::getInstance().setLevel(TRACE_LEVEL_DBG);
	Tracer::getInstance().addSink(std::make_shared<STDSink>()).start();
	if(!option.parseArgs(argc,argv))	/* 解析参数 */
	{
		return RC_ERROR;
	}
	if(option.hasOption("h")>=0)
	{
		TRACE_YELLOW("coroutine help:");
		TRACE_YELLOW("coroutine [-h]");
		return RC_OK;
	}

	CoScheduler scheduler;
	std::shared_ptr<CoroutinePing> coPing = std::make_shared<CoroutinePing>();
	std::shared_ptr<CoroutinePong> coPong = std::make_shared<CoroutinePong>();
	int coPingID = coPing->coroutineID();
	int coPongID = coPong->coroutineID();
	TRACE_DBG("pingID:%d, pongID:%d",coPingID,coPongID);	
	scheduler.start(coPing);
	scheduler.start(coPong);

	/* 在主进程调度协程 */
	while(1)
	{
		scheduler.schedule(coPingID);
		scheduler.schedule(coPongID);
	}
    return 0;
}
