var searchData=
[
  ['sched_5fpolicy_5ffifo_905',['SCHED_POLICY_FIFO',['../classlibemb_1_1_p_thread.html#ac9d64b44a031321d4ab3a2790259bddda539ad1068e9e17018e63246eb2ee1b3d',1,'libemb::PThread']]],
  ['sched_5fpolicy_5fother_906',['SCHED_POLICY_OTHER',['../classlibemb_1_1_p_thread.html#ac9d64b44a031321d4ab3a2790259bddda09c71044114782eba011166cbce6e593',1,'libemb::PThread']]],
  ['sched_5fpolicy_5frr_907',['SCHED_POLICY_RR',['../classlibemb_1_1_p_thread.html#ac9d64b44a031321d4ab3a2790259bddda22e3596e79010623b2761584dd92bf4b',1,'libemb::PThread']]],
  ['socket_5ftype_5fbroadcast_908',['SOCKET_TYPE_BROADCAST',['../_socket_8h.html#aa6016addaf077c4dd5dcf11f8def4169a37dd77135788444b993fe441d59492d2',1,'libemb']]],
  ['socket_5ftype_5fgroupcast_909',['SOCKET_TYPE_GROUPCAST',['../_socket_8h.html#aa6016addaf077c4dd5dcf11f8def4169a2296d85c7a3c2e448263e386bcf3aff0',1,'libemb']]],
  ['socket_5ftype_5fmulticast_910',['SOCKET_TYPE_MULTICAST',['../_socket_8h.html#aa6016addaf077c4dd5dcf11f8def4169a1129ad1d72fa46342dc8832a051a72b5',1,'libemb']]]
];
