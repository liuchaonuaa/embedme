var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwy",
  1: "abcdefilmnpqrstu",
  2: "abcdfilmnpst",
  3: "abcdefghiklmopqrstuvwy",
  4: "m",
  5: "fsu",
  6: "bcdfinprst",
  7: "bcirst",
  8: "o",
  9: "abcdegimprstuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Macros"
};

