# libemb API手册

由于作者时间有限，本手册仅对libemb库的部分API进行说明和示例，以便使用者能快速入门。

本手册只对类的使用进行阐述，不对如何创建工程和如何编译做过多的说明，如果想要了解编译系统的使用，请参考[build/README.md](../build/README.md)

如果想要了解更多和更深入的使用libemb，最好的办法是查看头文件注释及example下的例子，同样在test_case目录下你也可以看到对部分类的测试样例。


# 快速开始

## 第一个例子

例程：[demo.cpp](../app/demo/demo.cpp)

让我们从Hello开始吧：

``` cpp
#include "BaseType.h"
#include "Tracer.h"

using namespace std;
using namespace libemb;

int main()
{
    Tracer::getInstance().setLevel(TRACE_LEVEL_DBG);//设置打印信息级别
	Tracer::getInstance().addSink(std::make_shared<STDSink>()).start();//打印到标准输出
    TRACE_INFO("hello,world!");
    return 0;
}
```

## 日志打印

头文件：[Tracer.h](../opensource/libemb/Tracer.h)

Tracer是打印系统，用于调试输出或者日志输出，它是一个单例类，支持按级别打印,每个级别均有不同的颜色。
``` cpp
#define TRACE_LEVEL_DBG             0       /**< 用于调试信息的打印 */
#define TRACE_LEVEL_INFO            1       /**< 用于提示信息的打印 */
#define TRACE_LEVEL_REL             2       /**< 用于普通信息的打印 */
#define TRACE_LEVEL_WARN            3       /**< 用于警告信息打印 */
#define TRACE_LEVEL_ERR             4       /**< 用于错误信息的打印 */  
```
Tracer支持一系列宏：TRACE_DEBUG、TRACE_INFO、TRACE_REL、TRACE_ERR等等，具体定义查看[Tracer.h](../opensource/libemb/Tracer.h)

TracerSink是打印输出器，默认实现了STDSink和ROSSink。如果你想把打印输出到文件，你可以实现自己的TracerSink：
``` cpp
enum FILE_LOG_ID{
    FILE_LOG_DBG=100, //自定义的ID必须大于4
    FILE_LOG_INFO
};
class FileSink: public TracerSink{
private:
    File m_logFile;
public:
    FileSink()
    {
        m_logFile.open("log.txt",IO_MODE_REWR_ORNEW);
    }
    virtual ~FileSink(){};
    void sink(int uid, const std::string& msg) final
    {
        //注意：sink方法中不可以使用TRACE_XXX系列的宏,否则会死锁
        switch(uid){
        case FILE_LOG_DBG:
        case FILE_LOG_INFO:
            m_logFile.writeData(CSTR(msg),msg.size());
            break;
        }
    }
};
int main()
{
    Tracer::getInstance().setLevel(TRACE_LEVEL_DBG);
	Tracer::getInstance().addSink(std::make_shared<FileSink>()).start();
    //自定义日志打印输出器打印日志使用TRACE_EXT和TRACE_EXT_CLASS宏
    TRACE_EXT(FILE_LOG_DBG,"this is debug level message");
    TRACE_EXT(FILE_LOG_INFO,"this is info level message");
}
```

## 线程

头文件：[Thread.h](../opensource/libemb/Thread.h)

测试例程：[testThread.cpp](../test_case/test_libemb/testThread.cpp)

线程的创建有两种方法，一种方法是使用Runnable接口+Thread类，第二种方法是使用Threading类。

第一种方法：
``` cpp
class TestThread: public Runnable{
public:
    TestThread()
    {
        m_thread.start(*this);
    }
private:
    void run(Thread& thread)
    {
        while(thread.isRunning())
        {
            TRACE_DBG_CLASS("thread running.");
            Thread::msleep(1000);
        }
    }
private:
    Thread m_thread;
};
```
这种方法更适合于只创建单个线程的场景，用这种方法创建多个线程，那么多个线程将共用一个线程体。

第二种方法：
``` cpp
class TestThread:public Threading{
public:
    TestThread()
    {
        char* args = "hello";
        threading(&MultiThread::threadA,this,args);//使用std::thread线程化
        pthreading(&MultiThread::threadB,this,args);//使用pthread线程化
    }
private:
    void threadA(void* args)
    {
        while(1)
        {
            Thread::msleep(1000);
            TRACE_DBG_CLASS("run threadA().");
        }
    }
    void threadB(void* args)
    {
        while(1)
        {
            Thread::msleep(1000);
            TRACE_DBG_CLASS("run threadB().");
        }
    }
};
```
这是一种更加灵活的创建线程的方法，而且可以线程化当前类的任意一个方法。

Thread是采用std::thread实现的线程,使用比较简单。

PThread则是采用posix库实现的线程,用法跟Thread一样，不同的是它还可以设置线程调度策略和优先级等，可用于Preempt-RT系统中。


## 线程池

头文件：[Thread.h](../opensource/libemb/Thread.h)

测试例程：[testThread.cpp](../test_case/test_libemb/testThread.cpp)

ThreadPool线程池用法：
``` cpp
class MyTask:public Runnable{
public:
    void run(Thread& thread)
    {
        //do task
        while(thread.isRunning())
        {
            Thread::msleep(1000);
            std::cout<<"do task"<<std::endl;
        }
    }
};
int main()
{
    auto pool = ThreadPool::getInstance();
    Thread proto;//线程原型
    pool.init(proto,10);//生成10个proto线程
    MyTask task;
    for(int i=0; i<10; i++)
    {
        pool.start(task);//启动线程
    }
    for(int i=0; i<10; i++)
    {
        pool.cancel(i);//取消线程
    }
}
``` 

## 输入参数 (ArgUtil)

头文件：[ArgUtil.h](../opensource/libemb/ArgUtil.h)

ArgOption用于获取输入参数：

``` cpp
    ArgOption  argOption;
	/* 定义参数选项 */
	argOption.addOption("h", 0);	/* -h:帮助 */
	argOption.addOption("help", 0);	/* --help:帮助 */
	argOption.addOption("l", 1);	/* -l:打印级别,使用时必须跟一个参数: -l 2 */
	argOption.parseArgs(argc,argv);	/* 解析参数 */
    /* 判断参数 */
	if(argOption.getValue("h", argValue) || argOption.getValue("help", argValue))
	{
        //如果有"-h"或者"--help",打印帮助信息
        std::cout << "help info" << std::endl;
    }
    if (argOption.getValue("l", argValue))
    {
        //argValue为参数值
        std::cout << "-l: "<< argValue <<std::endl;
    }
```
InputReader用于读取终端输入：
``` cpp
int main()
{
    InputReader reader;
    while(1)
    {
        reader.waitInput();//等待用户输入
        if (reader.isString("q") || reader.isString("quit")) 
        {
            return;
        }
        if (reader.asString()=="q" || reader.asString()=="quit")//与上面写法效果一样
        {
            return;
        }
    }
}
```

## 协程 (Coroutine)

有时间再写，请先参考：[Coroutine.h](../opensource/libemb/Coroutine.h)

## 缓存 (DataBuffer)

有时间再写，请先参考：[DataBuffer.h](../opensource/libemb/DataBuffer.h)

## 时间日期 (DateTime)

有时间再写，请先参考：[DateTime.h](../opensource/libemb/DateTime.h)

## 协程 (Coroutine)

有时间再写，请先参考：[Coroutine.h](../opensource/libemb/Coroutine.h)

## 文件与目录 (FileUtil)

有时间再写，请先参考：[FileUtil.h](../opensource/libemb/FileUtil.h)

## 有限状态机 (FSMachine)

有时间再写，请先参考：[FSMachine.h](../opensource/libemb/FSMachine.h)

## IO复用 (Pollset)

有时间再写，请先参考：[Pollset.h](../opensource/libemb/Pollset.h)

## 进程 (ProcUtil)

有时间再写，请先参考：[ProcUtil.h](../opensource/libemb/ProcUtil.h)

## 串口 (SerialPort)

有时间再写，请先参考：[SerialPort.h](../opensource/libemb/SerialPort.h)

## 网络 (Socket)

有时间再写，请先参考：[Socket.h](../opensource/libemb/Socket.h)

## 字符串 (StrUtil)

有时间再写，请先参考：[StrUtil.h](../opensource/libemb/StrUtil.h)

## 线程通信 (ThreadUtil)

有时间再写，请先参考：[ThreadUtil.h](../opensource/libemb/ThreadUtil.h)

## 定时器 (Timer)

有时间再写，请先参考：[Timer.h](../opensource/libemb/Timer.h)
