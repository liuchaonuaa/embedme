#!/bin/bash
APP_NAME=app
valgrind --tool=memcheck --leak-check=full $PWD/output/bin/${APP_NAME}
