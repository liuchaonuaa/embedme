#!/bin/bash
###this file is generated by mbuild_project from mbuild system, see more in build/README.md
# invoke envsetup.sh, do not modify! 
PROJECT_ROOT=`cd $(dirname $0); pwd`
cd $PROJECT_ROOT
export AUTO_BUILD=yes
source build/envsetup.sh
rm autobuild.log

### set product and target
mbuild_setup 0 0 

### build target
mbuild_remake opensource/libemb libemb
mbuild_remake app/demo demo
mbuild_remake example/coroutine coroutine
mbuild_remake example/tcpcli tcpcli
mbuild_remake example/tcpsrv tcpsrv
mbuild_remake example/udpcli udpcli
mbuild_remake example/udpsrv udpsrv
mbuild_remake test_case/test_libemb test_libemb

### build libembx
BUILD_EMBX=false
if $BUILD_EMBX;then
mbuild_remake opensource/libconfig++ libconfig++
mbuild_remake opensource/libev libev
mbuild_remake opensource/libmqttclient libmqttclient
mbuild_remake opensource/libsocketcan libsocketcan
mbuild_remake opensource/libtinyxml libtinyxml
mbuild_remake opensource/spdlog spdlog
mbuild_remake opensource/libcjson libcjson
mbuild_remake opensource/libcurl libcurl
mbuild_remake opensource/libembx libembx
mbuild_remake test_case/test_libembx test_libembx
fi
