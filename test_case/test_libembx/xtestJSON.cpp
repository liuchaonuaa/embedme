#include <libemb/BaseType.h>
#include <libemb/Tracer.h>
#include <libemb/ArgUtil.h>
#include <libembx/JSONData.h>
#include <libemb/ProcUtil.h>
#include <libemb/Thread.h>

#if defined(OS_CYGWIN) || defined(OS_ANDROID)
#else
#include <libembx/HttpRequest.h>
#endif

using namespace libemb;
using namespace libembx;

static void testJSONWrite()
{
    /* 初始化一个对象型的JSONData*/
    JSONData jsonData;
    std::shared_ptr<JSONNode> rootNode=jsonData.initWithObjectFormat();

    /* 增加普通结点 */
    rootNode->addSubNode("empty");
    rootNode->addSubNode(false,"yes");
    rootNode->addSubNode(true,"no");
    rootNode->addSubNode(100,"int");
    rootNode->addSubNode(100.123,"double");
    rootNode->addSubNode("hello world.","string");

    /* 增加数组结点 */
    IntArray intArray;
    intArray.append(1).append(2).append(3).append(4).append(5);
    rootNode->addSubNode(intArray, "intArray");
    DoubleArray doubleArray;
    doubleArray.append(1.5).append(2.4).append(3.3).append(4.2).append(5.1);
    rootNode->addSubNode(doubleArray, "doubleArray");
    StringArray stringArray;
    stringArray.append("one").append("two").append("three").append("four").append("five");
    rootNode->addSubNode(stringArray, "stringArray");
    /* 保存到文件中 */
    jsonData.saveAsFile("object.json");

    /* 修改节点值 */
    (*rootNode)["yes"].setValue(false);
    (*rootNode)["no"].setValue(true);
    (*rootNode)["int"].setValue(999);
    (*rootNode)["double"].setValue(0.1234);
    (*rootNode)["string"].setValue("999");
    jsonData.saveAsFile("object_modify.json");

    /* 初始化一个数组型的JSONData*/
    JSONData arrayData;
    std::shared_ptr<JSONNode> arrayNode=arrayData.initWithArrayFormat();
    for(int i=0; i<3; i++)
    {
        JSONData data;
        std::shared_ptr<JSONNode> node = data.initWithObjectFormat();
        char buf[32]={0};
        sprintf(buf,"string%02d",i+1);
        node->addSubNode(buf, std::string(buf));
        arrayNode->addSubNode(std::move(node));
    }
    /* 保存到文件中 */
    arrayData.saveAsFile("array.json");
}

static void testJSONRead()
{
	
    JSONData json;
    const char* jsonString = "{\"version\":\"V1.0.0.1\",\"object\":{\"oid\":1234}}";
    std::shared_ptr<JSONNode> rootNode = json.initWithDataString(jsonString);
    JSONNode node = (*rootNode)["version"];
    TRACE_YELLOW("version:%s\n",CSTR(node.toString()));

	node = (*rootNode)["object"]["oid"];
    TRACE_YELLOW("oid:%d\n",node.toInt());
	
    node = (*rootNode)["object"].toNode();
    TRACE_YELLOW("---oid:%d\n",node["oid"].toInt());
    
}

#if 0
static void baiduDictTest(std::string word)
{
#if defined(OS_CYGWIN) || defined(OS_ANDROID)
	TRACE_ERR("Not support Dict Test.\n");
#else
    HttpClient client;
    std::string url="http://openapi.baidu.com/public/2.0/translate/dict/simple?client_id=";
    url += "faEkfGvdVpwjeQGms6HqzHaQ";
    url += "&q=";
    url += word;
    url +="&from=en&to=zh";
    File* file = NEW_OBJ File("/tmp/baidudict.cache",IO_MODE_REWR_ORNEW);
    client.httpGet(url,file);
    DEL_OBJ(file);

    JSONData json;
    JSONNode* rootNode=json.initWithContentOfFile("/tmp/baidudict.cache");
    
    #if 0   //显示json文本
    std::string out=json.serialize();
    TRACE_CYAN("%s\n",out.c_str());
    #endif
    
    /* 解析JSON */
    JSONNode node = (*rootNode)["errno"];
    if (node.toInt()==0)
    {
        node = (*rootNode)["data"]["word_name"];
        if(node.toString().empty())
        {
            TRACE_ERR("can't find word \"%s\"!!!\n",CSTR(word));
            return;
        }
        TRACE_CYAN("%s\n",CSTR(node.toString()));
        node=(*rootNode)["data"]["symbols"];
        JSONNode ele_node= node[0]["ph_am"];
        TRACE_CYAN("AM:[%s]\n",CSTR(ele_node.toString()));
        ele_node = node[0]["ph_en"];
        TRACE_CYAN("EN:[%s]\n",CSTR(ele_node.toString()));
        
        node = node[0]["parts"][0];
        ele_node= node["part"];
        TRACE_CYAN("%s\n",CSTR(ele_node.toString()));
        ele_node= node["means"];
        int arraySize = ele_node.getArraySize();
        for(int i=0;i<arraySize; i++)
        {
            TRACE_CYAN("%d.%s\n",i+1,CSTR(ele_node[i].toString()));
        }
    }
#endif
}
#endif
void testJSON(void)
{
    while (1)
    {
        TRACE_YELLOW("----JSONData Test----\n");
        TRACE_YELLOW("01: JSON write.\n");
        TRACE_YELLOW("02: JSON read.\n");
        TRACE_YELLOW("03: Baidu Dict test.\n");
        TRACE_YELLOW(" q: quit.\n");
        TRACE_YELLOW("please input comand:\n");
        InputReader reader;
		reader.waitInput();
        if (reader.isString("q")|| reader.isString("quit")) return;
        else if (reader.isString("01"))	testJSONWrite();
        else if (reader.isString("02"))	testJSONRead();
        else if (reader.isString("03"))
        {
            std::string word;
            TRACE_YELLOW("please input a English word:");
            std::cin >> word;
            //baiduDictTest(word);
            word.clear();
        }
        else
        {
            ;
        }
    }
}

