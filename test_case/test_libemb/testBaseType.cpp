#include <libemb/BaseType.h>
#include <libemb/Tracer.h>
#include <libemb/ArgUtil.h>

using namespace std;
using namespace libemb;
void testBaseType(void)
{
     while(1)
    {
    	InputReader reader;
        TRACE_DBG("-----------BaseType Test------------");
		TRACE_DBG("01: Array Test");
		TRACE_DBG("02: Tuple Test");
		TRACE_DBG("q : quit");
        reader.waitInput();
        if (reader.isString("q") || reader.isString("quit")) 
        {
            return;
        }
        else if(reader.isString("01")) 
        {
            IntArray intArray;
            intArray.initWithString("[1,2,3,4,5]");
            intArray.append(6).append(7).append(8).append(9).append(10);
            cout<<intArray.serialize()<<endl;
			cout<<"Array type:"<<intArray.type()<<",size:"<<intArray.size()<<endl;

            IntArray intArray2(std::string("[11,12,13,14,15]"));
            intArray2.append(16).append(17).append(18).append(19).append(20);
            cout<<intArray2.serialize()<<endl;
			cout<<"Array type:"<<intArray2.type()<<",size:"<<intArray2.size()<<endl;

            StringArray stringArray;
            stringArray.initWithString("[\"One\",\"Two\",\"Three\",\"Four\",\"Five\"]");
            stringArray.append("Six").append("Seven").append("Eight").append("Nine").append("Ten");
            cout<<stringArray.serialize()<<endl;
			cout<<"Array type:"<<stringArray.type()<<",size:"<<stringArray.size()<<endl;

            StringArray stringArray2(std::string("[\"One\",\"Two\",\"Three\",\"Four\",\"Five\"]"));
            stringArray2.append("Six").append("Seven").append("Eight").append("Nine").append("Ten");
            cout<<stringArray2.serialize()<<endl;     
			cout<<"Array type:"<<stringArray2.type()<<",size:"<<stringArray2.size()<<endl;
        }
		else if (reader.isString("02")) 
        {
            Tuple tuple;
			int i = 100;
			double d = 3.1415926;
            tuple.append("China").append(i).append(d);
            TRACE_YELLOW("print tuple:%s,%d,%f",CSTR(tuple[0].toString()),tuple[1].toInt(),tuple[2].toDouble());
            TRACE_YELLOW("print tuple:$s", CSTR(tuple.serialize())); 
            TRACE_YELLOW("print copy tuple:");
            Tuple tp(tuple);
            cout<<tp.serialize()<<endl; 
            TRACE_YELLOW("print equal tuple:");
            tp = tuple;
            cout<<tp.serialize()<<endl; 
        }
    }
}