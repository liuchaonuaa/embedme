#include <libemb/BaseType.h>
#include <libemb/Tracer.h>
#include <libemb/ArgUtil.h>
#include <libemb/Socket.h>
#include <libemb/Thread.h>
#include <libemb/DateTime.h>
#include <libemb/Timer.h>
using namespace libemb;

class RTimerTest:public TimerListener{
DECL_CLASSNAME(RTimerTest)
private:
	std::unique_ptr<RTimer> m_timer;
	Time m_lastTime;
public:
    RTimerTest()
    {
        /* 创建定时器 */
        m_timer = std::make_unique<RTimer>(*this,0x01);
    }
    ~RTimerTest()
    {
    }
    void onTimer(int timerId)
    {
    	Time currTime = Time::fromMono();
		Time diffTime = currTime-m_lastTime;
		int interval = diffTime.toMicroSec();
		TRACE_DBG_CLASS("Timer:%d at %s, interval:%d",timerId,CSTR(currTime.toString()),interval);
		m_lastTime = currTime;
    }
    void startTest()
    {
        /* 启用定时器 */
        m_timer->start(1000000,10000,true);
		m_lastTime = Time::fromMono();
    }
    void stopTest()
    {
        /* 停止定时器 */
        m_timer->stop();
        TRACE_DBG_CLASS("m_timer stopped.");
    }
};
void testTimer(void)
{
	TRACE_INFO("timer test start >>>>>>");
    std::unique_ptr<RTimerTest> test = std::make_unique<RTimerTest>();
    while(1)
    {
        TRACE_CYAN("====Test Menu====");
        TRACE_CYAN("01. start RTimer.");
        TRACE_CYAN("02. stop RTimer.");
        TRACE_CYAN("q . quit.");
        InputReader reader;
		reader.waitInput();
        if (reader.isString("q") || reader.isString("quit")) 
        {
            break;
        }
        else if(reader.isString("01"))
        {
            test->startTest();
        }
        else if(reader.isString("02"))
        {
            test->stopTest();
        }
    }
	test->stopTest();
	TRACE_INFO("timer test finished <<<<<<");
}

