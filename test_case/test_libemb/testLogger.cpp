#include <libemb/BaseType.h>
#include <libemb/Tracer.h>
#include <libemb/ArgUtil.h>
#include <libemb/Logger.h>
using namespace libemb;

void testLogger(void)
{
    TRACE_INFO("Logger test start >>>>>>");
	LoggerManager::getInstance().setRoot("./log");
	if (!LoggerManager::getInstance().createLogger("test",512,3))
	{
		TRACE_ERR("create logger error!");
		return;
	}
	
	for(auto i=0; i<10000; i++)
	{
		LOG_INFO("test","this is a test log.");
	}

    TRACE_INFO("Logger test finished <<<<<<");
}
