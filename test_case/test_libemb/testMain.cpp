/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#include <libemb/BaseType.h>
#include <libemb/Tracer.h>
#include <libemb/ArgUtil.h>

using namespace libemb;

extern void testBaseType(void);
extern void testTcp(void);
extern void testTimer(void);
extern void testUdp(void);
extern void testLogger(void);
extern void testFSMachine(void);
extern void testThread(void);

static void menu_print()
{
	TRACE_YELLOW("==================================================");
	TRACE_YELLOW("Embedme Module Test Build on %s %s",BUILD_DATE,BUILD_TIME);
	TRACE_YELLOW("==================================================");
	TRACE_YELLOW("01--> BaseType Test.");
    TRACE_YELLOW("02--> Tcp Test.");
    TRACE_YELLOW("03--> Udp Test.");
	TRACE_YELLOW("04--> Timer Test.");
	TRACE_YELLOW("05--> Logger Test.");
	TRACE_YELLOW("06--> FSMachine Test.");
	TRACE_YELLOW("07--> Thread Test.");
    TRACE_YELLOW("q --> quit test program.");
    TRACE_YELLOW("==================================================");
}
/* 测试主程序 */
int main(int argc,char* argv[])
{    
	std::string value;
    Tracer::getInstance().setLevel(TRACE_LEVEL_DBG);
	Tracer::getInstance().addSink(std::make_shared<STDSink>()).start();
	ArgOption option;
	if(!option.parseArgs(argc,argv))
	{
		return RC_ERROR;
	}
	if (option.hasOption("h")>=0)
	{
		TRACE_YELLOW("help info");
		FilePath filePath(argv[0]);
		TRACE_YELLOW("%s [-h] ",CSTR(filePath.baseName()));
		return RC_OK;	
	}

	InputReader reader;
	while(1)
    {
    	menu_print();	
		reader.waitInput();
		if(reader.isString("quit") || reader.isString("q"))return 0;
		else if(reader.isString("01"))	testBaseType();
		else if(reader.isString("02"))	testTcp();
		else if(reader.isString("03"))	testUdp();
		else if(reader.isString("04"))	testTimer();
		else if(reader.isString("05"))	testLogger();
		else if(reader.isString("06"))	testFSMachine();
		else if(reader.isString("07"))	testThread();
        else
        {
        	TRACE_RED("Unknown command: \"%s\"",reader.asCString());
        }
	}
	return 0;
}
