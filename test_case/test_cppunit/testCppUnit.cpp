#include <libemb/CppUnitLite.h>
using namespace libemb;
class Stack{
public:
    int size() 
   {
        return 0;
   }
};
//构建测试用例creationStackTest
TEST(Stack, creation)
{
    Stack s;
    LONGS_EQUAL(0, s.size());
    std::string b = "asa";
    CHECK_EQUAL("asa", b);
}

int main()
{
    TestResult tr;
    TestRegistry::runAllTests(tr);
    return 0;
}