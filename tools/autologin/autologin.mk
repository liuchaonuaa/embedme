LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := autologin

LOCAL_CFLAGS := -Wall
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_INC_PATHS := \
	$(LOCAL_PATH)

LOCAL_SRC_FILES := \
	autologin.c

include $(BUILD_EXECUTABLE)
