LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := killall

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS := -std=c++14
LOCAL_LDFLAGS := -lemb -lpthread -lrt -ldl

LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(PROJECT_ROOT)/opensource/libemb

LOCAL_SRC_FILES := \
	killall.cpp

include $(BUILD_EXECUTABLE)
