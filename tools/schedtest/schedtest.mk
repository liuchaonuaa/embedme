LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := schedtest

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS := -std=c++14
LOCAL_LDFLAGS := -lemb -lpthread -lrt -ldl
LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(PROJECT_ROOT)/opensource/libemb/inc

LOCAL_SRC_FILES := schedtest.cpp

include $(BUILD_EXECUTABLE)