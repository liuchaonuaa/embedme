LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libsdbus++

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS := -std=c++11
LOCAL_LDFLAGS :=

ifneq ($(HOST_FNAME),arm-linux-gnueabihf)
$(error "${LOCAL_MODULE} not support host: ${HOST_FNAME}")
exit 1
endif

LOCAL_INC_PATHS := \
    $(LOCAL_PATH) \
    $(PROJECT_ROOT)/libemb \
    $(PROJECT_ROOT)/libembx \
	$(PROJECT_ROOT)/openlibs/libsystemd-2.3.7/include

LOCAL_SRC_FILES := \
    SDBus.cpp

LOCAL_LIB_TYPE:=static

LOCAL_PRE_BUILD :=
LOCAL_POST_BUILD :=
include $(BUILD_LIBRARY)
