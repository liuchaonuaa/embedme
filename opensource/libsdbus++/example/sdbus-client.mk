LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := sdbus-client

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS := -lsdbus++ -llzma -lmount -lblkid -luuid -lcap -lsystemd
# 下面几个库是libsystemd-2.3.7的特有依赖
LOCAL_LDFLAGS += -llz4 -lgcrypt -lgpg-error
LOCAL_LDFLAGS += -lemb -lembx -lpthread -lrt -ldl

LOCAL_LIB_PATHS := $(PROJECT_ROOT)/openlibs/libsystemd-2.3.7/armhf-gcc7.5.0

LOCAL_INC_PATHS := \
	$(PROJECT_ROOT)/libemb \
	$(PROJECT_ROOT)/libembx \
	$(PROJECT_ROOT)/libsdbus++ \
	$(PROJECT_ROOT)/openlibs/libsystemd-2.3.7/include \
	$(LOCAL_PATH)/.. \
	$(LOCAL_PATH)

LOCAL_SRC_FILES := \
	sdbus-client.cpp


include $(BUILD_EXECUTABLE)
