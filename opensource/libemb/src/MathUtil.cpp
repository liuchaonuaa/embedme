/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/

#include "libemb/MathUtil.h"
#include "libemb/BaseType.h"

namespace libemb{

float MathUtil::deg2rad(float degree)
{
    while(degree<-180.0)
    {
        degree += 360.0;
    }
    while(degree>180.0)
    {
        degree -= 360.0;
    }
    return (degree/180.0*PI_FLTP32);
}

float MathUtil::rad2deg(float radian)
{
    while(radian<(-PI_FLTP32))
    {
        radian += (2.0*PI_FLTP32);
    }
    while(radian>(PI_FLTP32))
    {
        radian -= (2.0*PI_FLTP32);
    }
    return radian*180.0/PI_FLTP32;
}

}