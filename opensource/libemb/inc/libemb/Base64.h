/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __BASE64_H__
#define __BASE64_H__

#include "libemb/BaseType.h"

/** 
 * @file Base64.h
 * @brief BASE64编解码器头文件 
 */
namespace libemb{
/**
 * @class Base64
 * @brief BASE64编解码器
 */
class Base64{
DECL_CLASSNAME(Base64)
public:
	Base64(){};
	virtual ~Base64(){};
	/**
	 * @brief base64编码
	 * @param binDataIn 输入数据
	 * @param b64DataOut 输出数据
	 * @return true 编码成功
	 * @return false 编码失败
	 */
	bool b64Encode(const std::string& binDataIn,std::string& b64DataOut);
	/**
	 * @brief base64解码
	 * @param b64DataIn 输入数据
	 * @param binDataOut 输出数据
	 * @return true 解码成功
	 * @return false 解码失败
	 */
	bool b64Decode(const std::string& b64DataIn,std::string& binDataOut);
	/**
	 * @brief base64编码(URL安全)
	 * @param binDataIn 输入数据
	 * @param b64DataOut 输出数据
	 * @return true 编码成功
	 * @return false 编码失败
	 */
	bool b64EncodeUrlSafe(const std::string& binDataIn,std::string& b64DataOut);
	/**
	 * @brief base64解码(URL安全)
	 * @param b64DataIn 输入数据
	 * @param binDataOut 输出数据
	 * @return true 解码成功
	 * @return false 解码失败
	 */
	bool b64DecodeUrlSafe(const std::string& b64DataIn,std::string& binDataOut);
private:
	char findIndex(const char *str, char c);
};

} 
#endif

