/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __THREAD_SYNC_H__
#define __THREAD_SYNC_H__

#include "libemb/BaseType.h"
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <pthread.h>
#include <queue>

/**
 *  @file  ThreadUtil.h   
 *  @brief 线程工具	
 */
namespace libemb{
/**
 * @class Mutex
 * @brief 互斥锁类	
 * @note 互斥锁在lock后可以睡眠,但需注意unLock后再次lock之间必须给其他线程留有一定的抢占时间,否则当前线程会一直占着锁.
 */
class Mutex{
public:
    Mutex();
    ~Mutex();
    /**
     * @brief  互斥锁上锁
     * @return int 成功返回0,错误返回-1
     */
    int lock();
    /**
     * @brief  互斥锁解锁
     * @return int 成功返回0,错误返回-1
     */
    int unLock();
    /**
     * @brief  互斥锁尝试上锁
     * @return int 成功返回0,错误返回-1
     */
    int tryLock();
private:
    pthread_mutex_t m_mutex;
	friend class MutexCond;
};
/** 
 *  @class AutoLock
 *  @brief 自动释放的互斥锁类	
 */
class AutoLock{
public:
    AutoLock(Mutex& mutex);
    virtual ~AutoLock();
private:
    Mutex* m_pMutex;
};

/**
 *  @class MutexCond
 *  @brief 条件变量
 */
class MutexCond : public Mutex{
public:
    MutexCond();
    ~MutexCond();
    /**
     *  @brief  等待条件变量
     *  @param  usec,超时时间,<0时阻塞等待,>=0时等待usec
     *  @return 成功等到条件变量返回STATUS_OK,超时返回STATUS_TIMEOUT,失败返回STATUS_ERROR
     *  @code   多个线程不可以同时调用wait()方法,必需使用lock进行互斥,例程:
     *          线程A                              线程B
     *          ...                              ...
     *          cond.lock();               	     cond.lock();      
     *          while(pass==0)                   pass=1;
     *          {                                cond.meet();
     *             cond.wait(100);               con.unlock();
     *          }                                ...
     *          cond.unlock();
     *          ...
     * @endcode
     */
    int wait(int usec=-1);
    /**
     * @brief 通知等待者满足条件变量
     * @return int 
     */
    int meet();
private:
    pthread_cond_t m_cond;
    pthread_condattr_t m_condAttr;
};

/** 
 *  @class Semaphore
 *  @brief 信号量类(基于POSIX接口)	
 *  @note  无名信号量用于线程间通信,有名信号量用于进程间通信
 */
class Semaphore{
DECL_CLASSNAME(Semaphore)
public:
    /**
     *  @brief  信号量
     *  @param  name 名称中不能包含'/',name=NULL时表示创建无名信号量
     *  @note   分为无名信号量和有名信号量,无名信号量用于线程间通信,有名信号量用于进程间通信      
     */
    Semaphore(const char* name=NULL);
    ~Semaphore();
    /**
     *  @brief  打开信号量
     *  @param  value 只有有名信号量存在时,可以不指定该值
     *  @return 成功返回true,失败返回false
     *  @note   信号量存在,则返回,不存在则创建
     */
    bool open(int value=-1);
    /**
     *  @brief  关闭信号量
     *  @param  void
     *  @return 成功返回true,失败返回false
     *  @note   对于有名信号量,关闭信号量不会真正删除信号量,还需要调用unlink()
     */
    bool close();
    /**
     *  @brief  删除信号量
     *  @param  void
     *  @return 成功返回true,失败返回false
     *  @note   该方法仅对有名信号量有效
     */
	bool unlink();
    /**
     *  @brief  等待信号量
     *  @param  void
     *  @return 成功返回true,失败返回false
     *  @note   使信号量值减1,如果无资源可申请,则阻塞.
     */
    bool wait();
    /**
     *  @brief  尝试等待信号量
     *  @param  void
     *  @return 成功返回true,失败返回false
     *  @note   使信号量值减1,如果无资源可申请,则返回.
     */
    bool tryWait();
    /**
     *  @brief  释放信号量
     *  @param  void
     *  @return 成功返回true,失败返回false
     *  @note   使信号量值增加1,释放资源
     */
    bool post();
    /**
     *  @brief  获取信号量值
     *  @param  value 当前值
     *  @return 成功返回true,失败返回false
     *  @note   当value>0说明有资源,=0无资源,<0表示有|value|个线程在等待资源
     */
    bool getValue(int& value);
protected:
    sem_t m_sem;
	std::string m_name;
};

/** 
 *  @class SemaphoreV
 *  @brief 信号量类(基于systemV接口)	
 *  @note  使用systemV实现的进程间信号量
 */
class SemaphoreV{
DECL_CLASSNAME(SemaphoreV)
public:
	SemaphoreV();
	~SemaphoreV();
    /**
     * @brief 打开信号量
     * @param key 
     * @return true 
     * @return false 
     */
	bool open(int key);
    /**
     * @brief 关闭信号量
     */
	void close();
    /**
     * @brief 等待信号量
     * @return true 
     * @return false 
     */
	bool wait();
    /**
     * @brief 发送信号量
     * @return true 
     * @return false 
     */
	bool post();
    /**
     * @brief 获取信号量值
     * @param value 
     * @return true 
     * @return false 
     */
	bool getValue(int& value);
private:
	int m_sem;		
};
}

#endif
