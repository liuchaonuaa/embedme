/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MATH_UTIL_H__
#define __MATH_UTIL_H__

#define GA_FLTP32           (9.8)               /**< 重力加速度(32位). */
#define PI_FLTP32           (3.1415926)         /**< 圆周率(32位). */
#define PI_FLTP64           (3.141592653589793) /**< 圆周率(32位). */
#define SQUARE(x)       	((x)*(x))           /**< x 的平方. */
#define R_AREA(r)       	((PI)*(r)*(r))      /**< 计算半径为 r 的圆的面积. */

/**
 * @file MathUtil.h
 * @brief 数学工具
 */
namespace libemb{
/**
 * @class MathUtil
 * @brief 数学工具
 */
class MathUtil{
public:
    /**
     * @brief 角度转弧度
     * @param degree 
     * @return float 
     */
    static float deg2rad(float degree);
    /**
     * @brief 弧度转角度
     * @param radian 
     * @return float 
     */
    static float rad2deg(float radian);
};

}

#endif

