/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014-2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __POLLER_H__
#define __POLLER_H__

#include "libemb/BaseType.h"
#include "libemb/DateTime.h"
#include "libemb/Timer.h"
#include <sys/epoll.h>
#include <mutex>

/**
 * @file Poller.h
 * @brief IO复用/轮询
 */
namespace libemb{

class PollEvent{
public:
    enum EVENT_E{
        POLLIN = 0, //读
        POLLOUT,    //写
    };
public:
    PollEvent(){};
    PollEvent(int dev, int event):m_dev(dev),m_event(event){};
    int dev(){return m_dev;}
    int event(){return m_event;}
private: 
    int m_dev{-1};
    int m_event{-1};  
};


class PollDev{
public:
    PollDev(){};
    virtual ~PollDev(){};
    virtual bool reopen(){return false;}
    virtual PollEvent pollEvent()=0;
    virtual bool onPoll()=0;
};

/**
 * @class Poller 
 * @brief IO复用/轮询集
 */
class Poller{
DECL_CLASSNAME(Poller)
public:
	Poller();
	virtual ~Poller();
    /**
     * @brief 打开复用集
     * @param onceNotify 
     * @return true 
     * @return false 
     */
	bool open(int maxEvents, bool onceNotify=false);
    /**
     * @brief 关闭复用集
     * @param void
     */
	void close();
    /**
     * @brief 增加轮询事件
     * @param event 
     * @return true 
     * @return false 
     */
	bool addEvent(PollEvent& event);
    /**
     * @brief 移除轮询事件
     * @param event 
     * @return true 
     * @return false 
     */
	bool removeEvent(PollEvent& event);
    /**
     * @brief 等待事件
     * @param usTimeout 
     * @return 事件集
     */
	std::vector<std::shared_ptr<PollEvent>> waitEvent(int usTimeout);
private:
	int m_epfd{-1};
    int m_size{0};
	bool m_onceNotify{false};
    int m_eventNum{0};
	struct epoll_event* m_events{NULL};
    std::mutex m_mutex;
};

class EventPoller{
DECL_CLASSNAME(EventPoller)
public:
    EventPoller();
    virtual ~EventPoller();
    bool setup(int evMask=0xFFFFFFFF);
    void clearEvent();
    void meetEvent(int event);
    int waitEvent(int& event,int usTimeout,const std::vector<int>& evWait=std::vector<int>());
    
private:
    int m_epfd{-1};
    int m_rwfd[2]{-1,-1};
    int m_evMask{-1};
    struct epoll_event m_event{0};
};


/**
 *  @class  PollTimer
 *  @brief  基于事件实现的定时器类
 */
class PollTimer:public Runnable{
DECL_CLASSNAME(PollTimer)
public:
	/**
	 * @brief PollTimer构造函数
	 * @param listener 定时器监听器
	 * @param id 定时器ID
	 */
    PollTimer(const TimerListener& listener,int id);
    ~PollTimer();
	/**
	 * @brief 启动定时器
	 * @param usTimeout 定时时间
	 * @param repeat 是否重复
	 * @return true 启动成功
	 * @return false 启动失败
	 */
    bool start(int usTimeout,bool repeat=false);
	/**
	 * @brief 停止定时器
	 */
    void stop();
	/**
	 * @brief 获取定时器ID
	 * @return int 定时器ID
	 */
    int id(){return m_timerID;}
private:
    void run(Thread& thread);
private:
	std::unique_ptr<Thread> m_thread{nullptr};
	TimerListener* m_listener{nullptr};
    Poller m_poller;
    int m_tmfd{-1};
    int m_usInterval{0};
	int m_timerID{0};
};

}
#endif
