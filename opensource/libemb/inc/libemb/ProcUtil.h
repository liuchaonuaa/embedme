/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014-2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __COMMANDPIPE_H__
#define __COMMANDPIPE_H__

/**
 *  @file   ProcUtil.h
 *  @brief  进程工具类.
 */

#include "libemb/BaseType.h"
#include "libemb/IODevice.h"
#include "libemb/StrUtil.h"

namespace libemb{
/**
 * @class ProcUtil 
 * @brief 进程工具类
 */
class ProcUtil{
public:
    ProcUtil(){};
    ~ProcUtil(){};
    /**
     * @brief 执行命令
     * @param cmd 命令
     * @param resultStr 命令输出结果 
     * @param timeoutSec 超时时间(-1:等待进程退出,其他值:最长等待时间)
     * @return int 成功返回RC_OK,否则返回RC_ERROR
     */
    static int runCommand(std::string cmd, std::string& resultStr, int timeoutSec=-1);
     /**
     * @brief 执行命令
     * @param cmd 命令
     * @param timeoutSec 超时时间(-1:等待进程退出,其他值:最长等待时间)
     * @return std::string 命令输出结果
     */
    static std::string runCommand(std::string cmd, int timeoutSec=-1);
    /**
     * @brief 启动进程
     * @param procName 进程名
     * @param args 参数
     * @param envs 环境变量
     * @param delays 延时时间
     * @return int 成功返回RC_OK,否则返回RC_ERROR
     */
	static int startProc(const std::string& procName, char** args, int delays=0);
    /**
     * @brief 停止进程(杀死进程)
     * @param procName 进程名 
     */
	static void stopProc(std::string procName);
    /**
     * @brief 获取进程PID
     * @param processName 进程名称 
     * @return std::vector<int> 进程PID数组
     */
	static std::vector<int> getPidsByName(const std::string& processName);
    /**
     * @brief 获取环境变量
     * @param name 环境变量名称
     * @return std::string 
     */
	static std::string getEnv(const std::string& name);
    /**
     * @brief 设置环境变量
     * @param name 环境变量名称
     * @param value 环境变量值
     */
    static void setEnv(const std::string& name, const std::string& value);
    /**
     * @brief 设置进程亲和性(将当前进程/线程绑定到指定的cpu)
     * @param cpuMask 类型:bitmap,bit0代表cpu0,以此类推
     * @return true 
     * @return false 
     */
    static bool setAffinity(int cpuMask);
};

/**
 * @class ProcFifo 
 * @brief 进程间管道(有名管道)
 */
class ProcFifo:public IODevice{
DECL_CLASSNAME(ProcFifo)
public:
    ProcFifo();
    virtual ~ProcFifo();
    /**
     * @brief 打开有名管道
     * @param devName 管道名称
     * @param ioMode 设备模式(IO_MODE_E)
     * @return true 打开成功
     * @return false 打开失败
     */
    virtual bool open(const char *devName, int ioMode = IO_MODE_RDWR_ONLY); 
};
}
#endif
