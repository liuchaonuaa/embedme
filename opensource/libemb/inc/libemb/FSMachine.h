#ifndef __FS_MACHINE_H__
#define __FS_MACHINE_H__

#include "libemb/BaseType.h"
#include "libemb/Thread.h"
#include <map>
#include <set>
/**
 * @file FSMachine.h
 * @brief 状态机
 * @code 用法
 *    //状态机用法:
 *    //1.实例化StateHandler.
 *    //2.将状态处理器注册到状态机
 *    //3.设置初始状态,启动状态机.
 *    //4.通过改变状态控制状态机运行.
 *    FSMachine stateMachine;
 *    AStateHandler stateA;
 *    BStateHandler stateB;
 *    stateMachine.registerState(0,stateA);
 *    stateMachine.registerState(1,stateB);
 *    stateMachine.startup();
 *    stateMachine.changeState(0);
 * @endcode 
 */

namespace libemb{

class FSMachine;
/**
 * @class FSHandler 
 * @brief 状态处理器
 */
class FSHandler{
DECL_CLASSNAME(FSHandler)
public:
    FSHandler(){};
    virtual ~FSHandler(){};
    /**
     * @brief 状态处理函数
     * @return 返回下一个状态
     * @note 每一个子类都必须实现该方法
     */
    virtual int handleState()=0;
};

/**
 * @class FSListener 
 * @brief 状态监听器
 * @note 当状态机状态发生改变时,通知所有状态监听器
 */
class FSListener{
public:
    virtual void onStateChanged(int stateID)=0;
};

/**
 * @class FSMachine 
 * @brief 状态机
 */
class FSMachine:public Runnable{
DECL_CLASSNAME(FSMachine)
public:
    FSMachine();
    virtual ~FSMachine();
    /**
     * @brief 启动状态机
     * @param stateID 初始状态,默认为-1(非法状态)
     */
    bool startup(int stateID=-1);
    /**
     * @brief 退出状态机
     */
    void quit();
    /**
     * @brief 注册状态处理器
     * @param stateID,必须大于等于0，负数为非法状态
     * @param stateHandler 
     * @return true 
     * @return false 
     */
    bool registerState(int stateID,FSHandler& stateHandler);
    /**
     * @brief 注册状态监听器
     * @param listener 
     */
    void registerListener(FSListener& listener);

    /**
     * @brief 获取当前状态
     * @return int 
     */
    int currentState();

    /**
     * @brief 改变当前状态
     * @param stateID 
     * @return true 
     * @return false 
     */
    bool changeState(int stateID);
private:
    void run(Thread& thread);
private:
    Thread m_thread;
    Mutex m_stateMutex;
    int m_currState{-1};
    std::map<int,FSHandler*> m_stateHandlerMap;
    std::set<FSListener*> m_stateListenerSet;
};

}

#endif
