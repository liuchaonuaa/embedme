#ifndef __TRACER_LESS_H__
#define __TRACER_LESS_H__

/* 此文件用于消除打印,在实时系统中,为了满足系统实时性,我们有时有必要关闭串口打印.
 * 这种情况下，我们只需要在源码中包含本文件(在include "Tracer.h"之后),即可立即消
 * 除打印.示例代码:
 * #include "Tracer.h"
 * #include "TracerLess.h" //调试时我们可以屏蔽这句,那样会正常打印
 * ...
 * TRACE_WARN("..."); //当我们包含TracerLess.h时,此时将不打印
 * ...
 */

#ifdef TRACE_DBG
#undef TRACE_DBG  
#endif
#define TRACE_DBG(fmt,arg...)  

#ifdef TRACE_INFO
#undef TRACE_INFO 
#endif
#define TRACE_INFO(fmt,arg...)

#ifdef TRACE_DBG_CLASS
#undef TRACE_DBG_CLASS 
#endif
#define TRACE_DBG_CLASS(fmt,arg...)

#ifdef TRACE_INFO_CLASS
#undef TRACE_INFO_CLASS 
#endif
#define TRACE_INFO_CLASS(fmt,arg...)

#ifdef TRACE_HEX
#undef TRACE_HEX 
#endif
#define TRACE_HEX(tag,buf,len)

#ifdef TRACE_TEXT
#undef TRACE_TEXT 
#endif
#define TRACE_TEXT(fmt,arg...)

#ifdef TRACE_ASSERT
#undef TRACE_ASSERT 
#endif
#define TRACE_ASSERT(condition)

#endif
