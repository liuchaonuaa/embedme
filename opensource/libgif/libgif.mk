# this file is generated by mbuild_project from mbuild system, see more in prebuild/README.md
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

# module name
LOCAL_MODULE := libgif

# lib type: static or shared
LOCAL_LIB_TYPE := static

# build source directory
LOCAL_BUILD_DIR := libgif-5.1.0

# makefile in LOCAL_BUILD_DIR
LOCAL_BUILD_MAKEFILE := Makefile.mbuild

LOCAL_PRE_BUILD :=
LOCAL_POST_BUILD :=
include $(BUILD_FROMSRC)