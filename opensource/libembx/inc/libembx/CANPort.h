/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __CAN_PORT_H__
#define __CAN_PORT_H__

#include "libsocketcan.h"
#include "libemb/BaseType.h"

/**
 * @file CANPort.h
 * @brief CAN总线端口
 */

namespace libembx{

/**
 * @enum CANSTATE_E
 * @brief CAN端口状态
 */
enum CANSTATE_E
{
	CANSTATE_ERROR_ACTIVE=CAN_STATE_ERROR_ACTIVE,	/**< 激活状态(0):RX/TX error count < 96 */
	CANSTATE_ERROR_WARNING=CAN_STATE_ERROR_WARNING,	/**< 告警状态(1):RX/TX error count < 128 */
	CANSTATE_ERROR_PASSIVE=CAN_STATE_ERROR_PASSIVE,	/**< 被动状态(2):RX/TX error count < 256 */
	CANSTATE_BUS_OFF=CAN_STATE_BUS_OFF,				/**< 总线关闭(3):RX/TX error count >= 256 */
	CANSTATE_STOPPED=CAN_STATE_STOPPED,				/**< 停止(4):Device is stopped */
	CANSTATE_SLEEPING=CAN_STATE_SLEEPING,			/**< 睡眠(5):Device is sleeping */
};

/**
 * @class CANPort
 * @brief CAN总线端口类
 */
class CANPort{
DECL_CLASSNAME(CANPort)
public:
	CANPort();
	~CANPort();
	/**
	 * @brief 打开CAN端口
	 * @param name 端口名称
	 * @param bitrate 速率bps
	 * @return true 
	 * @return false 
	 */
	bool open(const char* name, int bitrate);
	/**
	 * @brief 关闭CAN端口
	 */
	void close();
	/**
	 * @brief 重启CAN端口
	 */
	void restart();
	/**
	 * @brief 获取CAN端口状态
	 * @return int 
	 */
	int getState();
private:
	std::string m_name;
};
}
#endif
