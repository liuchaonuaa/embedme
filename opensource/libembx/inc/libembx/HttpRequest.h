/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __HTTP_REQUEST_H__
#define __HTTP_REQUEST_H__

#ifdef OS_CYGWIN
#else
#include "libemb/BaseType.h"
#include "libemb/FileUtil.h"

namespace libembx{
using namespace libemb;
enum HTTP_ACK_E{
    HTTP_OK = 200,// OK: Success!
    HTTP_BAD_REQUEST = 400,// Bad Request: The request was invalid. 
    HTTP_NOT_AUTHORIZED = 401,// Not Authorized: Authentication credentials were missing or incorrect.
    HTTP_FORBIDDEN = 403,// Forbidden: The request is understood, but it has been refused.  
    HTTP_NOT_FOUND = 404,// Not Found: The URI requested is invalid or the resource requested, such as a user, does not exists.
    HTTP_NOT_ACCEPTABLE = 406,// Not Acceptable.
    HTTP_INTERNAL_SERVER_ERROR = 500,// Internal Server Error: Something is broken. 
    HTTP_BAD_GATEWAY = 502,// Bad Gateway
};

/**
 *  @file   HttpRequest.h   
 *  @class  HttpRequest
 *  @brief  HTTP请求.
 */
class HttpRequest{
DECL_CLASSNAME(HttpRequest)
public:
    HttpRequest();
    ~HttpRequest(); 
    int getRequest(const std::string url,File& cacheFile,int timeoutSeconds=20);
    int postRequest(const std::string url,const std::string postString,File& cacheFile,int timeoutSeconds=20);
    int fileRequest(const std::string url,File& cacheFile);
private:
	static int writeFuntion(void *ptr, int size, int nmemb, FILE *stream) ;
};
}
#endif
#endif
