/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://gitee.com/newgolo/embedme.git
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __STP_PACKER_H__
#define __STP_PACKER_H__

#include "BaseType.h"
#include "Thread.h"
#include <vector>

namespace libembx{
/**
 *  @file   STPPacker.h   
 *  @class  STPPacker
 *  @brief  一个简单的串口传输协议(Serial Transfer Protocol)封装器.
 *  @note   协议格式:帧标识(FEFE)+帧长度(2Bytes,不大于0x8000,不小于0x0006)+消息内容+校验码(2Bytes)
 *          如果消息内容或校验码中有FEFE,则增加一个FEFE进行转义
 */
class STPPacker{
DECL_CLASSNAME(STPPacker)
public:
    STPPacker();
    ~STPPacker();
    std::vector<std::string> unpackOn(const char* buf,int size);
    std::string packData(const char* data,int size);
private:
    std::string getPayload(const std::string& frame);
private:
    std::string m_dataBuffer;
    int m_frameStart;   /* 帧头位置 */
    int m_frameSize;    /* 帧长度 */
    int m_stpState;     /* 解包状态 */
};
}
#endif
