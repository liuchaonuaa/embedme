#include <libemb/BaseType.h>
#include <libemb/Tracer.h>

using namespace std;
using namespace libemb;

int main()
{
    Tracer::getInstance().setLevel(TRACE_LEVEL_DBG);
    Tracer::getInstance().addSink(std::make_shared<STDSink>()).start();
    TRACE_INFO("hello,world!");
    return 0;
}
